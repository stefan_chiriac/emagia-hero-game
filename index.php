<?php
require __DIR__ . '/vendor/autoload.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>eMagia - EverGreen Forest </title>
    <link href="resources/css/bootstrap.css" rel="stylesheet">
    <link href="resources/css/index.css" rel="stylesheet">
    <script src="vendor/components/jquery/jquery.min.js"></script>
    <script src="vendor/twbs/bootstrap/dist/js/bootstrap.js"></script>
    <script src="resources/js/index.js"></script>

    <link href="vendor/components/font-awesome/css/all.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col"></div>
        <div class="col">
            <img src="/resources/images/logo.png"/>
        </div>
        <div class="col"></div>
    </div>
</div>
<div class="container-sm">
    <div class="row">
        <div class="col-6 col-sm-4">
            <p>Hello traveler and
                Welcome to Ever-Green Forest of eMagia. I've been looking for a hero as strong as you for a long
                time. If you feel ready, tell me your name and let's get started!</p>
            <form method="post" class="custom-form" >
                <div class="form-row">
                    <div class="col-md-4 mb-3 offset-4">
                        <button class="form-control mt-3 bg-transparent border-dark" type="submit" name="submit" id="fight">Fight</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container custom">
    <?php
    if (!empty($_POST)) {
        require_once('src/GameBundle/Process/Application.php');
        try {
            $app = \GameBundle\Process\Application::getInstance();
            $app->run();
        } catch (\Exception $exception) {
            echo $exception->getMessage();
        }
    }
    ?>

    <script>
        if ($('.fighterPresentation').length) {
            $('.container-sm').addClass('hidden');
        }
    </script>
    <script>
        if ($('.fightWinner-Hero').length) {
            $('.card-img-top-hero').css('border', '1px solid green');
        }
    </script>
    <script>

        if ($('.fightWinner-Beast').length) {
            $('.card-img-top-beast').css('border', '1px solid green');
        }
    </script>
</div>
</body>
</html>
<?php

namespace SkillBundle\Interfaces;

interface AbstractSkillInterface
{
    public function isUsed(): bool;
}

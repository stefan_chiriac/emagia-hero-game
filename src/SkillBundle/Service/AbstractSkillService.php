<?php

namespace SkillBundle\Service;

use SkillBundle\Interfaces\AbstractSkillInterface;

abstract class AbstractSkillService implements AbstractSkillInterface
{
    /** @var $chanceOfSuccess */
    protected $chanceOfSuccess;

    public function __construct(int $chanceOfSuccess)
    {
        $this->chanceOfSuccess = $chanceOfSuccess;
    }

    public function isUsed(): bool
    {
        $chanceOfFail = mt_rand(1, 100);
        if ($this->chanceOfSuccess >= $chanceOfFail) {
            return true;
        }
        return false;
    }

    public function getSkillName(): string
    {
        return preg_replace('/([a-z])([A-Z])/', '$1 $2', (new \ReflectionClass($this))->getShortName());
    }
}

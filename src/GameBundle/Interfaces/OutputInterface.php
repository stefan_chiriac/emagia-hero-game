<?php

namespace GameBundle\Interfaces;

use CharacterBundle\Interfaces\AbstractCharacterInterface;

interface OutputInterface
{
    public function render(AbstractCharacterInterface $attacker, AbstractCharacterInterface $opponent, int $round, int $oldDefenderHealth);
}

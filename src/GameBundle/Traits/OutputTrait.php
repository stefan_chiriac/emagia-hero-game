<?php

namespace GameBundle\Traits;

use GameBundle\Interfaces\OutputInterface;

class OutputTrait
{
    /** @var OutputInterface */
    protected $output;

    public function getOutput(): OutputInterface
    {
        return $this->output;
    }

    public function setOutput(OutputInterface $output): self
    {
        $this->output = $output;

        return $this;
    }

    public function output($message)
    {
        return $this->getOutput()->output($message);
    }
}

<?php

namespace GameBundle\Service;

require 'vendor/autoload.php';
use GameBundle\Interfaces\LoggerInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LoggerService implements LoggerInterface
{
    public function logger($log)
    {
        if (!file_exists(__DIR__ . '/logs/battle.log')) {
            file_put_contents(__DIR__ . '/logs/battle.log', '');
        }

        $time = date('m/d/y h:iA', time());

        $contents = file_get_contents(__DIR__ . '/logs/battle.log');
        $contents .= "$time\t$log\r";


        file_put_contents(__DIR__ . '/logs/battle.log', $contents);
    }
}

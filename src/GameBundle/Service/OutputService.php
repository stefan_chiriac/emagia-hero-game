<?php

namespace GameBundle\Service;

use CharacterBundle\Interfaces\AbstractCharacterInterface;
use GameBundle\Interfaces\OutputInterface;

class OutputService implements OutputInterface
{
    public function render(AbstractCharacterInterface $attacker, AbstractCharacterInterface $opponent, int $round, int $oldDefenderHealth)
    {
        $html = '';
        if ($round == 1) {
            $html .= '<div class="fighterPresentation">
                <p class="border-white">Battleground statistics</p>
                <div class="card-group">';
            if ($attacker == 'Hero') {
                $html .= '<div class="card"><img src="resources/images/hero.png" class="card-img-top-hero"">';
            } else {
                $html .= '<div class="card"><img src="resources/images/beast.png" class="card-img-top-beast"">';
            }
            $html .= '<div class="card-body">
                <h5 class="card-title"> ' . $attacker . ' </h5>
                 <p class="card-text">
                 <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="' . $attacker->getHealth() . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $attacker->getHealth() . '%">' . $attacker->getHealth() . '</div>
                    </div>
                    <table class="table">
                    <tbody>
                    <tr>
                    <th scope="row">Strength</th>
                    <td>' . $attacker->getStrength() . '</td>
                    </tr>
                    <tr>
                    <th scope="row">Defence</th>
                    <td>' . $attacker->getDefence() . '</td>
                    </tr>
                    <tr>
                    <th scope="row">Speed</th>
                    <td>' . $attacker->getSpeed() . '</td>
                    </tr>
                    <th scope="row">Luck</th>
                    <td>' . $attacker->getLuck() . '</td>
                    </tr>
                    </tbody>
                    </table>
                    </p>
                    </div>
                    </div>
                    <div class="card">
                <img src="resources/images/vs.png" class="card-img-top"">
                <div class="card-body">
                <div class="card-text">
                <form method="post">
                <div class="form-row">
                    <div class="col-md-4 mb-3 offset-4">
                        <button class="form-control mt-3 bg-warning border-dark" type="submit" name="submit" id="fight"><i class="fa fa-redo"></i> </button>
                    </div>
                </div>
            </form>
</div>
                <h5 class="card-title mb-5">
                <p>
                
                <i class="fa fa-jedi mt-5"></i><br>
                <i class="fa fa-shield-alt mt-4"></i><br>
                <i class="fa fa-bolt mt-4"></i><br>
                <i class="fa fa-hat-wizard mt-4"></i><br>
                  
</p>
                </h5>
                </div>
            </div>
                    <div class="card">';
            if ($opponent == 'Hero') {
                $html .= '<div class="card"><img src="resources/images/hero.png" class="card-img-top-hero"">';
            } else {
                $html .= '<div class="card"><img src="resources/images/beast.png" class="card-img-top-beast"">';
            }
            $html .= '<div class="card-body">
                <h5 class="card-title">' . $opponent . '</h5>
                 <p class="card-text">
                 <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="' . $opponent->getHealth() . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $opponent->getHealth() . '%">' . $opponent->getHealth() . '</div>
                    </div>
                    <table class="table">
                    <tbody>
                    <tr>
                    <th scope="row"></i> Strength</th>
                    <td>' . $opponent->getStrength() . '</td>
                    </tr>
                    <tr>
                    <th scope="row">Defence</th>
                    <td>' . $opponent->getDefence() . '</td>
                    </tr>
                    <tr>
                    <th scope="row">Speed</th>
                    <td>' . $opponent->getSpeed() . '</td>
                    </tr>
                    <th scope="row">Luck</th>
                    <td>' . $opponent->getLuck() . '</td>
                    </tr>
                    </tbody>
                    </table>
                    </p>
                    </div>
                    </div>
                    </div>
                   
                
                    </div>
                </div>';


            $html .= "<div class='container'>";
        }
        $html .= '
            <div class="divTable">
                <div class="divTableBody">
                    <div class="divTableRow">
                        <div class="divTableCellRound">Round ' . $round . '</div>
                    </div>
                    <div class="divTableRow">
                        <div class="divTableCell">' . $attacker . ' <a class="custom-bold-red">attacks</a> ' . $opponent . '</div>
                    </div>
                    <div class="divTableRow">
                        <div class="divTableCell">';

        if ($opponent->getLucky()) {
            $html .= $opponent . ' has a chance of luck and does not receive damage this turn! ';
        } else {
            if (!empty($attacker->getSkillsUsed())) {
                $html .= 'In this turn ' . $attacker . ' got luck and used the following skills: ' . PHP_EOL;
                foreach ($attacker->getSkillsUsed() as $skill) {
                    $html .= $skill . PHP_EOL;
                }
            }

            if (!empty($opponent->getSkillsUsed())) {
                $html .= 'In this turn ' . $opponent . ' got luck and used the following skills: ' . PHP_EOL;
                foreach ($opponent->getSkillsUsed() as $skill) {
                    $html .= $skill . PHP_EOL;
                }
            }

            $html .= $opponent . ' lost ' . max($oldDefenderHealth - $opponent->getHealth(), 0) . ' HP , remaining with ' . $opponent->getHealth() . ' HP.' . PHP_EOL;
        }

        $html .= '
                        </div>
                    </div>
                </div>
            </div>
       ';

        if ($opponent->getHealth() == 0) {
            $html .= '<div class="container">
                        <div class="fightWinner-' . $attacker . '" id="fightWinner-' . $attacker . '">
                    <p>The winner of the Epic Battle from Ever-Green Forest is ' . $attacker . '!!</p>
            </div>
            </div>';
        }

        if ($round == 20 && $opponent->getHealth() > 0) {
            $html .= '<div class="container">
                        <div class="fightWinner">
                    <p>A dense fog appeared in the Ever-Green Forest and the battle ended without a winner :( .</p>
            </div>
            </div>';
        }

        echo $html;
    }
}

<?php

namespace GameBundle\Process;

use ArenaBundle\Service\EverGreenForest;
use CharacterBundle\Entity\Hero;
use CharacterBundle\Entity\Beast;

class Application
{
    private static $_instance = null;

    private function __construct()
    {
        spl_autoload_register(function ($class) {
            $class = str_replace("\\", DIRECTORY_SEPARATOR, $class);
            require_once "src/{$class}.php";
        });
    }

    public function run()
    {
        $arena = new EverGreenForest();
        $hero = new Hero();
        $beast = new Beast();
        $arena->fight($hero, $beast);
    }

    public static function getInstance()
    {
        if (self::$_instance == null) {
            self::$_instance = new Application();
        }

        return self::$_instance;
    }
}

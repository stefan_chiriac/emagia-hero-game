<?php

namespace ArenaBundle\Interfaces;

use CharacterBundle\Interfaces\AbstractCharacterInterface;

interface AbstractArenaInterface
{
    public function fight(AbstractCharacterInterface $firstFighter, AbstractCharacterInterface $secondFighter);
}

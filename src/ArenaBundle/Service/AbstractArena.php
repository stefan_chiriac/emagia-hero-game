<?php

namespace ArenaBundle\Service;

use ArenaBundle\Interfaces\AbstractArenaInterface;
use CharacterBundle\Interfaces\AbstractCharacterInterface;
use GameBundle\Service\OutputService;

abstract class AbstractArena implements AbstractArenaInterface
{
    /** @var $numberOfRounds */
    private $numberOfRounds;
    /** @var AbstractCharacterInterface */
    private $firstAttacker;
    /** @var AbstractCharacterInterface */
    private $secondAttacker;
    /** @var $htmlOutput */
    private $htmlOutput;

    public function __construct(int $numberOfRounds)
    {
        $this->htmlOutput = new OutputService();
        $this->setNumberOfRounds($numberOfRounds);
    }

    public function fight(AbstractCharacterInterface $firstFighter, AbstractCharacterInterface $secondFighter)
    {
        $this->setAttackerPosition($firstFighter, $secondFighter);

        for ($i = 1; $i <= $this->getNumberOfRounds(); $i++) {
            if ($i % 2 == 0) {
                $oldDefenderHealth = $this->firstAttacker->getHealth();

                $this->secondAttacker->attack($this->firstAttacker);

                $this->htmlOutput->render(
                    $this->secondAttacker,
                    $this->firstAttacker,
                    $i,
                    $oldDefenderHealth
                );
            } else {
                $oldDefenderHealth = $this->secondAttacker->getHealth();

                $this->firstAttacker->attack($this->secondAttacker);

                $this->htmlOutput->render(
                    $this->firstAttacker,
                    $this->secondAttacker,
                    $i,
                    $oldDefenderHealth
                );
            }
            if ($this->firstAttacker->getHealth() == 0 || $this->secondAttacker->getHealth() == 0) {
                break;
            }
        }
    }

    protected function setNumberOfRounds(int $numberOfRounds)
    {
        if (isset($numberOfRounds) && filter_var($numberOfRounds, FILTER_VALIDATE_INT, array('options' => array('min_range' => 1)))) {
            $this->numberOfRounds = $numberOfRounds;
        } else {
            throw new \Exception("Need to set a proper value for the number of the rounds");
        }
    }

    private function setAttackerPosition(AbstractCharacterInterface $firstFighter, AbstractCharacterInterface $secondFighter)
    {
        $this->firstAttacker = $firstFighter;
        $this->secondAttacker = $secondFighter;
        if ($firstFighter->getSpeed() < $secondFighter->getSpeed()) {
            $this->firstAttacker = $secondFighter;
            $this->secondAttacker = $firstFighter;
        } elseif ($firstFighter->getSpeed() == $secondFighter->getSpeed()) {
            if ($firstFighter->getLuck() < $secondFighter->getLuck()) {
                $this->firstAttacker = $secondFighter;
                $this->secondAttacker = $firstFighter;
            }
        }
    }

    public function getNumberOfRounds()
    {
        return $this->numberOfRounds;
    }
}

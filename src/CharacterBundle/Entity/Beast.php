<?php

namespace CharacterBundle\Entity;

use CharacterBundle\Interfaces\AbstractCharacterInterface;

class Beast extends AbstractCharacter
{
    const HEALTH_RANGE = [60,90];
    const STRENGTH_RANGE = [60,90];
    const DEFENCE_RANGE = [40,60];
    const SPEED_RANGE = [40,60];
    const LUCK_RANGE = [25,40];

    public function __construct()
    {
        parent::__construct(self::HEALTH_RANGE, self::STRENGTH_RANGE, self::DEFENCE_RANGE, self::SPEED_RANGE, self::LUCK_RANGE);
    }

    public function attack(AbstractCharacterInterface $opponent)
    {
        if ($opponent->luckGenerator()) {
            return;
        }
        parent::attack($opponent);
    }

    public function defend(int $power)
    {
        parent::defend($power);
    }

    public function getSkillsUsed(): array
    {
        return array();
    }
}

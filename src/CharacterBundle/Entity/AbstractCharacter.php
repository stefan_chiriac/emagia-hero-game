<?php

namespace CharacterBundle\Entity;

use CharacterBundle\Interfaces\AbstractCharacterInterface;
use ReflectionClass;
use ReflectionException;

abstract class AbstractCharacter implements AbstractCharacterInterface
{
    /** @var $name */
    private $name;
    /** @var $health */
    private $health;
    /** @var $defence */
    private $defence;
    /** @var $speed */
    private $speed;
    /** @var $luck */
    private $luck;
    /** @var $strength */
    private $strength;

    /** @var bool */
    private $getLucky = false;

    public function __construct(array $healthRange, array $strengthRange, array $defenceRange, array $speedRange, array $luckRange)
    {
        $this->health = $this->setStrenghts($healthRange, "health");
        $this->strength = $this->setStrenghts($strengthRange, "strength");
        $this->defence = $this->setStrenghts($defenceRange, "defence");
        $this->speed = $this->setStrenghts($speedRange, "speed");
        $this->luck = $this->setStrenghts($luckRange, "luck");
    }

    protected function setStrenghts(array $range, string $propertyName): int
    {
        if ($this->checkRange($range)) {
            return rand($range[0], $range[1]);
        } else {
            throw new \Exception("Your hero must possess some " . $propertyName . " in range (e.g.: [x,y])");
        }
    }

    private function checkRange(array $range): bool
    {
        if (isset($range) && is_array($range) && count($range) == 2 && filter_var($range[0], FILTER_VALIDATE_INT, array('options' => array('min_range' => 0))) && filter_var($range[1], FILTER_VALIDATE_INT, array('options' => array('min_range' => 0)))) {
            return true;
        }
        return false;
    }

    public function luckGenerator(): bool
    {
        $luck = mt_rand(0, 100);
        if ($this->luck <= $luck) {
            $this->getLucky = true;
            return true;
        }
        $this->getLucky = false;
        return false;
    }

    public function attack(AbstractCharacterInterface $opponent)
    {
        $opponent->defend($this->strength);
    }

    public function defend(int $power)
    {
        $this->health = max($this->health - max($power - $this->defence, 0), 0);
    }

    public function getSpeed(): int
    {
        return $this->speed;
    }

    public function getLuck(): int
    {
        return $this->luck;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function getDefence(): int
    {
        return $this->defence;
    }

    public function getLucky(): bool
    {
        return $this->getLucky;
    }

    public function __toString(): string
    {
        try {
            return preg_replace('/([a-z])([A-Z])/', '$1 $2', (new ReflectionClass($this))->getShortName());
        } catch (ReflectionException $e) {
        }
        return $e;
    }
}

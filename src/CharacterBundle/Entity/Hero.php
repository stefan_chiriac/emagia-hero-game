<?php

namespace CharacterBundle\Entity;

use CharacterBundle\Interfaces\AbstractCharacterInterface;
use SkillBundle\Service\MagicShield;
use SkillBundle\Service\RapidStrike;

class Hero extends AbstractCharacter
{
    const RAPID_STRIKE_CHANCE = 10;
    const MAGIC_SHIELD_CHANCE = 20;
    const HEALTH_RANGE = [70, 100];
    const STRENGTH_RANGE = [70, 80];
    const DEFENCE_RANGE = [45, 55];
    const SPEED_RANGE = [40, 50];
    const LUCK_RANGE = [10, 30];

    /** @var $rapidStrike */
    private $rapidStrike;
    /** @var $magicShield */
    private $magicShield;
    /** @var array $usedSkills */
    private $usedSkills = array();

    public function __construct()
    {
        $this->rapidStrike = new RapidStrike(self::RAPID_STRIKE_CHANCE);
        $this->magicShield = new MagicShield(self::MAGIC_SHIELD_CHANCE);
        parent::__construct(self::HEALTH_RANGE, self::STRENGTH_RANGE, self::DEFENCE_RANGE, self::SPEED_RANGE, self::LUCK_RANGE);
    }

    public function attack(AbstractCharacterInterface $opponent)
    {
        if ($opponent->luckGenerator()) {
            return;
        }

        $this->resetSkillsUsed();

        if ($this->rapidStrike->isUsed()) {
            $this->usedSkills[] = $this->rapidStrike->getSkillName();
            parent::attack($opponent);
        }
        parent::attack($opponent);
    }

    public function defend(int $power)
    {
        $this->resetSkillsUsed();
        if ($this->magicShield->isUsed()) {
            $this->usedSkills[] = $this->magicShield->getSkillName();
            $power = intval(round($power/2));
        }
        parent::defend($power);
    }

    private function resetSkillsUsed()
    {
        $this->usedSkills = array();
    }

    public function getSkillsUsed(): array
    {
        return $this->usedSkills;
    }
}

<?php

namespace CharacterBundle\Interfaces;

interface AbstractCharacterInterface
{
    public function attack(self $defender);

    public function defend(int $power);

    public function luckGenerator(): bool;

    public function getSpeed(): int;

    public function getLuck(): int;

    public function getHealth(): int;

    public function getStrength(): int;

    public function getDefence(): int;

    public function getLucky(): bool;

    public function getSkillsUsed(): array;

    public function __toString(): string;
}

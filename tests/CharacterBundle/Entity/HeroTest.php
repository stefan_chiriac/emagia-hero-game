<?php

namespace tests\CharacterBundle\Entity;

use CharacterBundle\Entity\Hero;
use PHPUnit\Framework\TestCase;


class HeroTest extends TestCase
{
    public function testStatsRanges()
    {
        $character = new Hero();

        $health = $character->getHealth();
        $strength = $character->getStrength();
        $defence = $character->getDefence();
        $speed = $character->getSpeed();
        $luck = $character->getLuck();

        $this->assertGreaterThanOrEqual(70, $health);
        $this->assertGreaterThanOrEqual(70, $strength);
        $this->assertGreaterThanOrEqual(45, $defence);
        $this->assertGreaterThanOrEqual(40, $speed);
        $this->assertGreaterThanOrEqual(10, $luck);
    }
}
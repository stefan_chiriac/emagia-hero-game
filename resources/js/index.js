function Parchment(elementId, backgroundImage = "tan") {
    function Triangular(lo, mode, hi) {
        var Fmode = (mode - lo) / (hi - lo),
            U = Math.random();
        if (U < Fmode) {
            return lo + Math.sqrt(U * (hi - lo) * (mode - lo));
        } else {
            return hi - Math.sqrt((1 - U) * (hi - lo) * (hi - mode));
        }
    }

    var content = document.getElementById(elementId).innerHTML;
    document.getElementById(elementId).innerHTML = "";
    var width = document.getElementById(elementId).offsetWidth,
        height = document.getElementById(elementId).offsetHeight,
        paper = new Raphael(elementId, width, height),
        verticalMargin = width * 0.05,
        horizontalMargin = height * 0.05,
        pathString = "M0,0",
        x = 0,
        y = 0;
    //draw left column
    for (var i = 0; i < height; i++) {
        y = i;
        x = x + Triangular(-verticalMargin * 0.1, 0, verticalMargin * 0.1);
        if (x < 1) {
            x = 1;
        }
        if (x > verticalMargin) {
            x = verticalMargin;
        }
        pathString = pathString + "L" + String(x) + "," + String(y);
    }
    //draw bottom row
    for (var i = x; i < width; i++) {
        x = i;
        y = y + Triangular(-horizontalMargin * 0.1, 0, horizontalMargin * 0.1);
        if (y < height - horizontalMargin) {
            y = height - horizontalMargin;
        }
        if (y > height) {
            y = height;
        }
        pathString = pathString + "L" + String(x) + "," + String(y);
    }
    //draw right column
    for (var i = y; i > 0; i--) {
        y = i;
        x = x + Triangular(-verticalMargin * 0.1, 0, verticalMargin * 0.1);
        if (x < width - verticalMargin) {
            x = width - verticalMargin;
        }
        if (x > width) {
            x = width;
        }
        pathString = pathString + "L" + String(x) + "," + String(y);
    }
    //draw top row
    for (var i = x; i > 1; i--) {
        x = i;
        y = y + Triangular(-horizontalMargin * 0.1, 0, horizontalMargin * 0.1);
        if (y < 1) {
            y = 1;
        }
        if (y > horizontalMargin) {
            y = horizontalMargin;
        }
        pathString = pathString + "L" + String(x) + "," + String(y);
    }
    pathString = pathString + "L0,0";
    paper.path(pathString).attr("fill", backgroundImage);

    var newdiv = document.createElement("div");
    newdiv.style.position = "absolute";
    newdiv.style.top = "0px";
    newdiv.style.left = "0px";
    newdiv.style.zIndex = "100";
    newdiv.style.paddingTop = horizontalMargin + "px";
    newdiv.style.paddingLeft = verticalMargin + "px";
    newdiv.style.paddingRight = verticalMargin + "px";
    newdiv.style.paddingBottom = horizontalMargin + "px";
    newdiv.innerHTML = content;
    document.getElementById(elementId).appendChild(newdiv);
}

window.onload = Parchment("testbox", "url(https://i.imgur.com/ivuyWNh.jpg)");//url(pathToImage.jpg) or rgb(R,G,B)



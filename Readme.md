![Alt text](resources/images/logo.png?raw=true "Title")


# Introduction

Once upon a time there was a great hero, called Orderus, with some strengths and weaknesses, as all heroes have.

## Hero Stats

After battling all kinds of monsters for more than a hundred years, Orderus now has the following stats:

```bash
# Health: 70 - 100
# Strength: 70 - 80
# Defence: 45 – 55
# Speed: 40 – 50
# Luck: 10% - 30%
```

## Beast Stats

```python
# Health: 60 - 90
# Strength: 60 - 90
# Defence: 40 – 60
# Speed: 40 – 60
# Luck: 25% - 40%
```

## Side Notes
```
vendor\bin\phpunit tests # Run unit tests
```
eMagia - EverGreen Forest is a browser game. Before you play it, don't forget to do nothing and enjoy :).